FROM centos:latest
MAINTAINER Nineparadise "polasin.int@gmail.com"

RUN yum update -y 
RUN dnf install nodejs -y
RUN yum -y install gcc-c++ make
RUN yum -y install -y git
RUN yum -y install epel-release
RUN yum -y install bzip2
RUN ln -sf /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin
#RUN yum -y groupinstall "Chinese Support"
RUN yum -y groupinstall "fonts"
RUN npm install -g bower 
RUN npm install -g pm2
RUN yum install pango.x86_64 libXcomposite.x86_64 libXcursor.x86_64 libXdamage.x86_64 \
                libXext.x86_64 libXi.x86_64 libXtst.x86_64 cups-libs.x86_64 libXScrnSaver.x86_64 \
                libXrandr.x86_64 GConf2.x86_64 alsa-lib.x86_64 atk.x86_64 gtk3.x86_64 \
                xorg-x11-fonts-100dpi xorg-x11-fonts-75dpi xorg-x11-utils xorg-x11-fonts-cyrillic xorg-x11-fonts-Type1 xorg-x11-fonts-misc 
RUN echo 'kernel.unprivileged_userns_clone=1' > /etc/sysctl.d/00-local-userns.conf
RUN systemctl restart procps 
RUN useradd umeal
COPY ./ /home/umeal/restaurant-pos/
RUN chown -R umeal:umeal /home/umeal/
WORKDIR /home/umeal/restaurant-pos
RUN rm package-lock.json ; exit 0
USER umeal
RUN npm i pupeteer
RUN npm install
CMD ["pm2-runtime","index.js"]